use std::convert::TryFrom;
use std::fmt;
use std::mem;
use std::num;
use std::sync::mpsc::{Receiver, SyncSender};
use std::sync::{Arc, Mutex};
use Opcode::*;

pub struct ComputerState {
    pub memory: [u16; 1 << 15],
    pub registers: Arc<Mutex<[u16; 8]>>,
    pub stack: Vec<u16>,
    pub ip: u16,
}

#[repr(u16)]
#[allow(dead_code)]
#[derive(Debug)]
pub enum Opcode {
    Halt = 0,
    Set = 1,
    Push = 2,
    Pop = 3,
    Eq = 4,
    Gt = 5,
    Jmp = 6,
    Jt = 7,
    Jf = 8,
    Add = 9,
    Mult = 10,
    Mod = 11,
    And = 12,
    Or = 13,
    Not = 14,
    Rmem = 15,
    Wmem = 16,
    Call = 17,
    Ret = 18,
    Out = 19,
    In = 20,
    Noop = 21,
}

#[derive(Debug, Clone)]
pub struct InvalidOpError {
    pub op: u16,
}
impl fmt::Display for InvalidOpError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "Invalid operator, {}.", self.op)
    }
}
impl std::error::Error for InvalidOpError {}

impl TryFrom<u16> for Opcode {
    type Error = InvalidOpError;
    fn try_from(val: u16) -> Result<Opcode, Self::Error> {
        match val {
            0..=21 => Ok(unsafe { mem::transmute(val) }),
            _ => Err(InvalidOpError { op: val }),
        }
    }
}

impl ComputerState {
    fn operand(&mut self) -> u16 {
        let val = self.memory[self.ip as usize];
        self.ip += 1;

        match val {
            0..=32767 => val,
            32768..=32775 => self.registers.lock().unwrap()[(val - 32768) as usize],
            _ => panic!(),
        }
    }

    fn register(&mut self) -> usize {
        let val = self.memory[self.ip as usize] - 32768;
        self.ip += 1;
        val as usize
    }

    pub fn eval(
        &mut self,
        output_sender: SyncSender<u16>,
        input_receiver: Receiver<u16>,
    ) -> anyhow::Result<()> {
        loop {
            if self.ip == 5489 {
                println!("{:?}", self.registers.lock().unwrap());
            }

            let operator = Opcode::try_from(self.operand())?;

            match operator {
                Halt => return Ok(()),
                Set => {
                    let register = self.register();
                    let value = self.operand();
                    self.registers.lock().unwrap()[register] = value;
                }
                Push => {
                    let a = self.operand();
                    self.stack.push(a);
                }
                Pop => {
                    let a = self.register();
                    self.registers.lock().unwrap()[a] = self.stack.pop().unwrap();
                }
                Eq => {
                    let a = self.register();
                    let b = self.operand();
                    let c = self.operand();
                    self.registers.lock().unwrap()[a] = (b == c) as u16;
                }
                Gt => {
                    let a = self.register();
                    let b = self.operand();
                    let c = self.operand();
                    self.registers.lock().unwrap()[a] = (b > c) as u16;
                }
                Jmp => self.ip = self.operand(),
                Jt => {
                    let condition = self.operand();
                    let location = self.operand();
                    if condition != 0 {
                        self.ip = location;
                    }
                }
                Jf => {
                    let condition = self.operand();
                    let location = self.operand();
                    if condition == 0 {
                        self.ip = location;
                    }
                }
                Add => {
                    let a = self.register();
                    let b = self.operand();
                    let c = self.operand();
                    self.registers.lock().unwrap()[a] =
                        (num::Wrapping(b) + num::Wrapping(c)).0 & !(1 << 15);
                }
                Mult => {
                    let a = self.register();
                    let b = self.operand();
                    let c = self.operand();
                    self.registers.lock().unwrap()[a] =
                        (num::Wrapping(b) * num::Wrapping(c)).0 & !(1 << 15);
                }
                Mod => {
                    let a = self.register();
                    let b = self.operand();
                    let c = self.operand();
                    self.registers.lock().unwrap()[a] = b % c;
                }
                And => {
                    let a = self.register();
                    let b = self.operand();
                    let c = self.operand();
                    self.registers.lock().unwrap()[a] = b & c & !(1 << 15);
                }
                Or => {
                    let a = self.register();
                    let b = self.operand();
                    let c = self.operand();
                    self.registers.lock().unwrap()[a] = b | c & !(1 << 15);
                }
                Not => {
                    let a = self.register();
                    let b = self.operand();
                    self.registers.lock().unwrap()[a] = !b & !(1 << 15);
                }
                Rmem => {
                    let register = self.register();
                    let memory_address = self.operand() as usize;
                    self.registers.lock().unwrap()[register] = self.memory[memory_address];
                }
                Wmem => {
                    let memory_address = self.operand() as usize;
                    let register = self.operand();
                    self.memory[memory_address] = register;
                }
                Call => {
                    let location = self.operand();
                    self.stack.push(self.ip);
                    self.ip = location;
                }
                Ret => self.ip = self.stack.pop().unwrap(),
                Out => output_sender.send(self.operand())?,
                In => {
                    let a = self.register();
                    let input = input_receiver.recv()?;
                    self.registers.lock().unwrap()[a] = input;
                }
                Noop => continue,
            };
        }
    }
}
