use std::convert::TryFrom;
use std::fs;
use std::slice;
use synacor;

fn main() -> anyhow::Result<()> {
    let binary = fs::read("data/challenge.bin")?;

    let program: Vec<u16> = unsafe {
        let len = binary.len() / 2;
        let u16_slice = slice::from_raw_parts(binary.as_ptr() as *const _, len);
        Vec::from(u16_slice)
    };

    for (index, &val) in program.iter().enumerate() {
        match synacor::Opcode::try_from(val) {
            Ok(op) => print!("\n{}: {:?}", index, op),
            Err(synacor::InvalidOpError { op }) => match op {
                0..=32767 => print!(" {}", op),
                _ => print!(" reg{}", op - 32768 + 1),
            },
        }
    }

    Ok(())
}
