use std::env;
use std::fs;
use std::io::{self, BufRead};

fn main() -> anyhow::Result<()> {
    let mut args = env::args();
    let binary = fs::File::open(args.nth(1).unwrap())?;
    let reader = io::BufReader::new(binary);

    let mut last_line_prompt = false;
    for line in reader.lines() {
        let line = line?;

        if last_line_prompt {
            println!("{}", line);
        }

        last_line_prompt = line == "What do you do?";
    }

    Ok(())
}
