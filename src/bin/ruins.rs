use itertools::Itertools;

fn main() {
    let nums: Vec<usize> = vec![
        2, // red
        9, // blue
        3, // corroded
        7, // concave
        5, // shiny
    ];

    for perm in nums.into_iter().permutations(5) {
        let out = perm[0] + perm[1] * perm[2].pow(2) + perm[3].pow(3) - perm[4];

        if out == 399 {
            println!("{:?} {}", perm, out);
        }
    }
}
