use std::fs;
use std::io;
use std::slice;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;
use synacor;

fn main() -> anyhow::Result<()> {
    let binary = fs::read("data/challenge.bin")?;

    let mut memory = [0u16; 1 << 15];
    unsafe {
        let len = binary.len() / 2;
        let u16_program = slice::from_raw_parts(binary.as_ptr() as *const _, len);
        memory[..len].copy_from_slice(u16_program);
    };

    let registers = Arc::new(Mutex::new([0u16; 8]));
    let stack = vec![];
    let ip = 0;

    let mut state = synacor::ComputerState {
        memory,
        registers: registers.clone(),
        stack,
        ip,
    };

    let (input_sender, input_receiver) = mpsc::sync_channel::<u16>(0);
    let (output_sender, output_receiver) = mpsc::sync_channel::<u16>(0);

    let program_thread = thread::spawn(move || state.eval(output_sender, input_receiver));
    thread::spawn(move || -> anyhow::Result<()> {
        loop {
            let mut string = String::new();
            io::stdin().read_line(&mut string)?;

            if string.len() >= 4 && &string[..4] == "regs" {
                println!("{:?}", registers.lock().unwrap());
            } else if string.len() >= 4 && &string[..4] == "set8" {
                registers.lock().unwrap()[7] = string[5..string.len() - 1].parse()?;
            } else {
                for c in string.chars() {
                    input_sender.send(c as u16)?;
                }
            }
        }
    });
    thread::spawn(move || -> anyhow::Result<()> {
        loop {
            let output = output_receiver.recv()?;
            print!("{}", output as u8 as char);
        }
    });

    program_thread.join().unwrap()?;

    Ok(())
}
